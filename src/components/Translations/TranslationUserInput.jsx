
const TranslateUserInput = (props) => {

    const {letter} = props

    return (
        <>
            <div key={letter.id}>
                <img className="imageSign" src={"images/" + letter.letter + ".png"} alt="" width="80"/>
            </div>
        </>
    )
}
export default TranslateUserInput