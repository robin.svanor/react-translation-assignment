
const TranslationForm = () => {


    return (
        <>
            <div className="translateInput">
                <fieldset className="fieldset">
                    <span>
                        <img className="keyboard" src='logoImages/keyboardSimple.png' alt='' width="40" height="40"/>
                    </span>
                    <span>
                        <img className="line" src='logoImages/line.png' alt='' width="40" height="35"/>
                    </span>
                    <input className="input" type="text" onChange={""} placeholder="What do you wish to translate?"></input>
                    <button className="translateButton" onClick={""}>Translate</button>
                </fieldset>
            </div>
            <div className="translatedOutput">
                <fieldset className="translatedFieldsetBox"></fieldset>
            </div>
        </>
    )
}
export default TranslationForm