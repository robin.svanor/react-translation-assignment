import { useState, useEffect } from 'react'
import {useForm} from 'react-hook-form'
import { storageSave } from '../../utils/storage'
import {loginUser} from '../api/user'
import {useNavigate} from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {

    // Hooks
    const {register, handleSubmit, formState: {errors}} = useForm()
    const {user, setUser} = useUser()
    const navigate = useNavigate()


    // Local State
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)



    // Side Effects
    useEffect(() => {
        if (user !== null) {
            navigate("/profile")
        }
        console.log("User has changed!", user)
    }, [user, navigate])


    // Event Handler
    const onSubmit = async ({username}) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }


    // Render Function
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if (errors.username.type === 'minLength') {
            return <span>Username must be three characters or longer</span>
        }
    })()

    return (
        <>
            <h2>Get started</h2>
            <img className="logoHello" src={"logoImages/Logo-Hello.png"} alt="" width="200"/>
            <div className="loginInput">
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset className="fieldset">
                    <div className="inputField">
                    <span>
                        <img className="keyboard" src='logoImages/keyboardSimple.png' alt='' width="40" height="40"/>
                    </span>
                    <span>
                        <img className="line" src='logoImages/line.png' alt='' width="40" height="35"/>
                    </span>
                    <input
                        className="input"
                        type="text"
                        placeholder="What is your name?"
                        {...register("username", usernameConfig)} />
                    <button className="loginButton" type="submit" disabled={loading}>login</button>
                    </div>
                        {errorMessage}
                </fieldset>

                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
            </form>
            </div>
        </>
    )
}

export default LoginForm