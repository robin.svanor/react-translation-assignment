import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({translations}) => {

    const translationList = translations.map((translation, index) => <ProfileTranslationHistoryItem key={index + "-" + translation} translation={translation} />)

    return (
        <>
            <h4>Your Translation History</h4>
            <fieldset className="translationHistoryFieldset">
                <section>
                    <ul>{translationList}</ul>
                </section>
            </fieldset>
        </>
    )
}
export default ProfileTranslationHistory