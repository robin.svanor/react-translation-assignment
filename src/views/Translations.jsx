import TranslationForm from "../components/Translations/TranslationFrom"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"

const Translations = () => {

    const {user} = useUser()

    return (
        <>
            <h1>Translations</h1>
            <section id="translate-output">
                <TranslationForm />
            </section>
        </>
    )
}

export default withAuth(Translations)