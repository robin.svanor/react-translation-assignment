import LoginForm from "../components/Login/LoginForm"

const Login = () => {
    return (
        <>
            <h1>Lost In Translation</h1>
            <LoginForm />
        </>
    )
}

export default Login